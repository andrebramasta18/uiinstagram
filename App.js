import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StatusBar,
  FlatList,
  useWindowDimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { FlatGrid } from 'react-native-super-grid';
const App = () => {
  const window = useWindowDimensions();
  const [data, setData] = useState([
    {
      title: 'Boleh',
      description: 'minta nmr Wa nya gak',
      imageUrl:
        'https://images.unsplash.com/photo-1668613962615-992eef4ec71e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=464&q=80',
    },
    {
      title: 'ketenangan ku',
      description: 'ada dendam yang dalam',
      imageUrl:
        'https://images.unsplash.com/photo-1666849490884-5dd04fc1ee8a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=464&q=80',
    },
    {
      title: 'manusia',
      description: 'wajah tanah',
      imageUrl:
        'https://images.unsplash.com/photo-1671128728034-abfbdaf0cad6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=415&q=80',
    },
    {
      title: 'sedang',
      description: 'menikmati angin',
      imageUrl:
        'https://images.unsplash.com/photo-1671454264323-ceb82eec84e3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
    },
    {
      title: 'pagi',
      description: 'yang cerah',
      imageUrl:
        'https://images.unsplash.com/photo-1672295270083-e494e9b614e3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
    },
    {
      title: '',
      description: 'Healing',
      imageUrl:
        'https://images.unsplash.com/photo-1672295270076-00b8eeb24d8c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
    },
  ]);

  return (
    <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
      <StatusBar barStyle={'dark-content'} backgroundColor="#ffffff" />
      <View style={{ marginHorizontal: 20, marginTop: 20, flexDirection: 'row' }}>
        <View>
          <Image
            source={{
              uri: 'https://images.unsplash.com/photo-1672412198777-b74b098715a2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
            }}
            style={{ width: 60, height: 60, borderRadius: 60 / 2 }}
          />
          <Text style={{ fontSize: 12, marginTop: 10 }}>@gusnando</Text>
          <Text style={{ fontWeight: 'bold', color: '#212121' }}>
            Andre Adi S{' '}
            <Icon name="check-circle" solid size={12} color="#59c5f4" />
          </Text>
        </View>

        <View
          style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center' }}>
          <Text style={{ color: '#212121' }}>Post</Text>
          <Text style={{ fontWeight: 'bold', color: '#212121' }}>6</Text>
        </View>
        <View
          style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center' }}>
          <Text style={{ color: '#212121' }}>Pengikut</Text>
          <Text style={{ fontWeight: 'bold', color: '#212121' }}>66,2jt</Text>
        </View>
        <View
          style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center' }}>
          <Text style={{ color: '#212121' }}>Mengikuti</Text>
          <Text style={{ fontWeight: 'bold', color: '#212121' }}>9</Text>
        </View>
      </View>

      <View style={{ marginHorizontal: 20 }}>
        <Text style={{color:'#000000'}}>Hpnc capter pamekasan 👊</Text>
        <Text style={{ color: '#000000' }}>Sang Editor Alight Motion</Text>
      </View>
      <View style={{ flexDirection: 'row', marginHorizontal: 20, marginTop: 20 }}>
        <TouchableOpacity
          style={{
            flex: 1,
            paddingVertical: 12,
            backgroundColor: '#FFFFFF',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 3,
            elevation: 2,
            marginRight: 5,
          }}>
          <Text style={{ color: '#59c5f4', fontWeight: 'bold' }}>Unfollow</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 1,
            paddingVertical: 12,
            backgroundColor: '#59c5f4',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 3,
            elevation: 2,
            marginLeft: 5,
          }}>
          <Text style={{ color: '#FFFFFF', fontWeight: 'bold' }}>Follow</Text>
        </TouchableOpacity>
      </View>

      <View style={{ flexDirection: 'row', marginTop: 20, marginHorizontal: 20 }}>
        <TouchableOpacity
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Icon name="image" solid size={18} color="#59c5f4" />
          <Text style={{ fontWeight: 'bold',color:'#000000' }}>Photo</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Icon name="video" solid size={18} color="#59c5f4" />
          <Text style={{ color: '#000000' }}>Videos</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Icon name="tags" solid size={18} color="#59c5f4" />
          <Text style={{color:'#000000'}}>Tagged</Text>
        </TouchableOpacity>
      </View>

      <View style={{ flex: 1, marginHorizontal: 20, marginTop: 20 }}>
        <FlatGrid
          data={data}
          itemDimension={100}
          spacing={0}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={{
                marginHorizontal: 5,
                marginTop: 5,
                borderRadius: 6,
                backgroundColor: '#FFFFFF',
                elevation: 2,
                paddingVertical: 10,
                marginBottom: 5,
              }}>
              <Image
                style={{
                  height: 100,
                  borderRadius: 6,
                }}
                source={{ uri: item.imageUrl }}
              />
              <View style={{ marginHorizontal: 5 }}>
                <Text style={{ color: '#121212' }}>{item.title}</Text>
                <Text style={{ color: '#121212' }}>{item.description}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          borderTopWidth: 1,
          borderTopColor: '#bdbdbd',
        }}>
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="home" solid size={18} color="#59c5f4" />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="image" solid size={18} color="#59c5f4" />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="video" solid size={18} color="#59c5f4" />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
            backgroundColor: '#59c5f4',
          }}>
          <Icon name="user" solid size={18} color="#FFFFFF" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default App;
